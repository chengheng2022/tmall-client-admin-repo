import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from 'axios';
import qs from 'qs';
import wangEditor from 'wangeditor';
import BaseUrl from "@/http/BaseUrl";
import SimpleAxios from "@/http/SimpleAxios";
import TokenAxios from "@/http/TokenAxios";

Vue.prototype.TokenAxios = TokenAxios;
Vue.prototype.SimpleAxios = SimpleAxios;
Vue.prototype.BaseUrl = BaseUrl;
Vue.prototype.wangEditor = wangEditor;
Vue.prototype.axios = axios;
Vue.prototype.qs = qs;
Vue.use(ElementUI);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
