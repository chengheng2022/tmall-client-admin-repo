import axios from 'axios';
import qs from 'qs';

let instance = axios.create();

const SimpleAxios = {
    get(url, params) {
        return instance({
            method: 'GET',
            url: url,
            data: params
        });
    },
    post(url, params) {
        return instance({
            method: 'POST',
            url: url,
            data: qs.stringify(params)
        });
    }
}

export default SimpleAxios;