import axios from 'axios';
import qs from 'qs';
import router from '@/router/index.js';

let instance = axios.create();

instance.interceptors.request.use((config) => {
    let loginInfoString = localStorage.getItem('loginInfo');
    if (!loginInfoString) {
        router.push('/login');
        return;
    }
    config.headers.Authorization = JSON.parse(loginInfoString).token;
    return config;
})

const TokenAxios = {
    get(url, params) {
        return instance({
            method: 'GET',
            url: url,
            data: params
        });
    },
    post(url, params) {
        return instance({
            method: 'POST',
            url: url,
            data: qs.stringify(params)
        });
    }
}

export default TokenAxios;